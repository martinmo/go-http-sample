FROM golang:1.9.7-alpine AS build-env

ADD ./src /src
WORKDIR /src

RUN go test
RUN go build -o goapp



# build runtime image
FROM alpine:3.7

COPY --from=build-env /src/goapp /goapp
ENTRYPOINT /goapp
