package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"strconv"
)

func TestHandler_OK(t *testing.T) {
	req, _ := http.NewRequest("GET", "?summand1=1.1&summand2=2.1", nil)
	w := httptest.NewRecorder()
	handler(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Valid request did not return %v", http.StatusOK)
	}

	result,_ := strconv.ParseFloat(w.Body.String(),64)
	if result != 3.2{
		t.Errorf("Valid request did not return correct result but %s",w.Body.String())
	}
}

func TestHandler_MissingQueryParam(t *testing.T) {
	req, _ := http.NewRequest("GET", "?summand1=1.1", nil)
	w := httptest.NewRecorder()
	handler(w, req)
	if w.Code != http.StatusBadRequest {
		t.Errorf("Request with missing query param did not return correct return code %v", http.StatusBadRequest)
	}
}

func TestHandler_InvalidQueryParam(t *testing.T) {
	req, _ := http.NewRequest("GET", "?summand1=INVALID&summand2=2.1", nil)
	w := httptest.NewRecorder()
	handler(w, req)
	if w.Code != http.StatusBadRequest {
		t.Errorf("Request with missing query param did not return correct return code %v", http.StatusBadRequest)
	}
}

func BenchmarkHandler(t *testing.B) {
	req, _ := http.NewRequest("GET", "?summand1=1.1&summand2=2.1", nil)
	w := httptest.NewRecorder()
	t.ResetTimer()
	handler(w, req)
	if w.Code != http.StatusOK {
		t.Errorf("Valid request did not return %v", http.StatusOK)
	}

	result,_ := strconv.ParseFloat(w.Body.String(),64)
	if result != 3.2{
		t.Errorf("Valid request did not return correct result but %s", w.Body.String())
	}
}