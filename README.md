Demo of a very simple HTTP server written in Go and running in a docker container.

On Windows replace "C:\Work\go-http" with your working directory. If not just use "$PWD".

Build
=====

Create an image which contains the compiled app

``docker build -t go-http .``

Run container

``docker run -it --rm --name go-http -p 8080:8080 go-http``

Go to [http://localhost:8080/?summand1=1.1&summand2=2.1](http://localhost:8080/?summand1=1.1&summand2=2.1)

Interactive
===========

``docker run --rm -it -v "C:\Work\go-http\app":/usr/src/app -w /usr/src/app golang:1.9.7 bash``


Test
----
``go test``

``go test -bench .``

Compile only
============

Compile Linux binary

``docker run --rm -v "$PWD"/app:/usr/src/app -w /usr/src/app golang:1.9.7 go build -v``

Compile Linux binary on Windows (adjust path)

``docker run --rm -v "C:\Work\go-http\app":/usr/src/app -w /usr/src/app golang:1.9.7 go build -v``

Cross Compile
-------------

Compile Windows binary

``docker run -e GOOS=windows -e GOARCH=386 --rm -v "$PWD"/app:/usr/src/app -w /usr/src/app golang:1.9.7 go build -v``

(on Windows: ``docker run -e GOOS=windows -e GOARCH=386 --rm -v "C:\Work\go-http\app":/usr/src/app -w /usr/src/app golang:1.9.7 go build -v``)

See [https://golang.org/doc/install/source#environment](https://golang.org/doc/install/source#environment) for other targets.
